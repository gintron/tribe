from django.shortcuts import render, redirect, get_object_or_404
from django.http import HttpResponse, HttpResponseRedirect
from .forms import RegisterForm, EditProfileForm, EditProfileImage, CreateTribeForm, PlaylistForm, MessageForm, TrackForm, CommentForm
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth import login, authenticate
from django.contrib.auth import logout
from .models import Profile, Tribe, PlayList, Message, Track, Vote, Comment
from django.contrib.auth.decorators import login_required
from django.urls import reverse

def index(request):
    if request.user.is_authenticated:
        my_tribes = Tribe.objects.all().filter(user = request.user)
        member_tribes = Tribe.objects.all().filter(member = request.user)
        free_tribes = Tribe.objects.all().exclude(user = request.user).exclude(member = request.user)
        context = {
            "my_tribes": my_tribes,
            "member_tribes" : member_tribes,
            "free_tribes" : free_tribes
        }
        return render (request, "home.html", context)
    else:
        all_tribes = Tribe.objects.all()
        context = {
            "all_tribes" : all_tribes
        }
        return render(request, "home.html", context)
   

def register(request):
	if request.method == "POST":
		form = RegisterForm(request.POST)
		if form.is_valid():
			user = form.save()
			Profile.objects.create(
				user=user,
				)
			login(request, user)
			return redirect('index')	
	form = RegisterForm
	return render (request=request, template_name="account/register.html", context={"register_form":form})

def loginF(request):
    if request.method == 'POST':
        form = AuthenticationForm(request=request, data=request.POST)
        if form.is_valid():
            username = form.cleaned_data.get('username')
            password = form.cleaned_data.get('password')
            user = authenticate(username=username, password=password)
            if user is not None:
                login(request, user)
                return redirect('/')
    form = AuthenticationForm()
    return render(request = request,
                    template_name = "account/login.html",
                    context={"form":form})

def logoutF(request):
	logout(request)
	return redirect('index')

@login_required
def profile(request):
    profile = request.user.profile
    form = EditProfileForm(instance=request.user)
    form_image = EditProfileImage(instance=profile)

    if request.method == 'POST':
        form = EditProfileForm(request.POST, instance=request.user)
        form_image = EditProfileImage(request.POST, instance=profile)
        if form.is_valid() and form_image.is_valid():
            form.save()
            form_image.save()
            return redirect(reverse('profile'))
        else:
            return redirect(reverse('profile'))

    context = {
        'form': form,
        'form_image': form_image 
    }
    return render(request, 'account/profile.html', context)

@login_required
def createTribe(request):
    form = CreateTribeForm
    if request.method == 'POST':
        form = CreateTribeForm(request.POST)
        if form.is_valid():
            tribe = form.save(commit=False)
            tribe.user = request.user
            tribe.save()
            return redirect(reverse('index'))
    context = {
        'form' : form
    }
    return render(request, 'create_tribe.html', context)

@login_required
def joinTribe(request, tribe_id):
    tribe = get_object_or_404(Tribe, pk=tribe_id)
    user = request.user

    if request.method == 'POST':
        tribe.member.add(user)
        tribe.save()
    return redirect(reverse('index'))   

def tribeDetail(request, tribe_id):
    tribe = get_object_or_404(Tribe, pk=tribe_id)
    playlists = PlayList.objects.all().filter(tribe = tribe_id)
    members = tribe.member.all()
    messages = Message.objects.all().filter(tribe=tribe_id)
    context = {
        'tribe' : tribe,
        'playlists' : playlists,
        'members': members,
        'messages' : messages
    }
    return render(request, 'detail_tribe.html', context)

@login_required
def createPlaylist(request, tribe_id):
    form = PlaylistForm(request.POST or None)
    tribe = get_object_or_404(Tribe, pk=tribe_id)
    if form.is_valid():
        playlist = form.save(commit=False)
        playlist.user = request.user
        playlist.tribe = tribe
        playlist.save()
        return redirect(reverse('tribeDetail', kwargs={'tribe_id' : tribe.id }))
    context = {
        "form" : form
    }
    return render(request, 'create_playlist.html', context)

@login_required
def deletePlaylist(request, tribe_id, playlist_id):
    playlist = get_object_or_404(PlayList, pk=playlist_id)
    tribe = get_object_or_404(Tribe, pk=tribe_id)
    if request.method == 'POST' and request.user.is_authenticated:
        playlist.delete()
    return redirect(reverse('tribeDetail', kwargs={'tribe_id':tribe.id}))

@login_required
def editPlaylist(request, tribe_id, playlist_id):
    tribe = get_object_or_404(Tribe, pk=tribe_id)
    playlist = get_object_or_404(PlayList, pk=playlist_id)
    if request.method == 'POST':
        form = PlaylistForm(request.POST, instance = playlist)
        if form.is_valid():
            form.save()
            return redirect(reverse('tribeDetail', kwargs={'tribe_id':tribe.id}))
    form = PlaylistForm(instance=playlist)
    context = {'form': form, 'playlist' : playlist, 'tribe':tribe}
    return render(request, 'edit_playlist.html', context)    

@login_required
def kickMember(request, tribe_id, member_name):
    tribe = get_object_or_404(Tribe, pk=tribe_id)
    playlists = PlayList.objects.all().filter(tribe=tribe_id)
    members = tribe.member.all()
    messages = Message.objects.all().filter(tribe=tribe_id)
    userToBeRemoved = tribe.member.get(username = member_name)
    if request.method == 'POST' and tribe.user == request.user:
        tribe.member.remove(userToBeRemoved)
        tribe.save()
        return redirect(reverse('tribeDetail', kwargs={'tribe_id':tribe_id}))
    context = {
        'tribe':tribe,
        'playlists':playlists,
        'members':members,
        'messages' : messages
    }
    return render(request, 'detail_tribe.html', context)

@login_required
def leaveTribe(request, tribe_id, userName):
    tribe = get_object_or_404(Tribe, pk=tribe_id)
    playlists = PlayList.objects.all().filter(tribe = tribe_id)
    members = tribe.member.all()
    messages = Message.objects.all().filter(tribe=tribe_id)
    userToBeRemoved = tribe.member.get(username = userName)
   

    if request.method == 'POST':
        tribe.member.remove(userToBeRemoved)
        tribe.save()
        return redirect(reverse('tribeDetail', kwargs={'tribe_id' : tribe.id}))
    
    context = {
        'tribe':tribe,
        'playlists' : playlists,
        'members' : members,
        'messages' : messages
    }

    return render(request, 'detail.tribe.htmml', context)

@login_required
def sendMessage(request, tribe_id):
    tribe = get_object_or_404(Tribe, pk=tribe_id)
    plalists = PlayList.objects.all().filter(tribe = tribe_id)
    members = tribe.member.all()
    form = MessageForm(request.POST)

    if request.method == 'POST':
        if form.is_valid():
            message = form.save(commit=False)
            message.user = request.user
            message.tribe = tribe
            message.profile_pic = request.user.profile.profile_pic
            message.save()
            return redirect(reverse('tribeDetail', kwargs={'tribe_id':tribe_id}))

    context = {
        'tribe': tribe,
        'playlists': plalists,
        'members':members,
        'messeges': Message.objects.all().filter(tribe = tribe_id),
        'form': form
    }
    return render(request, 'detail_tribe.html', context)

@login_required
def deleteMessage(request, tribe_id, message_id):
    tribe = get_object_or_404(Tribe, pk=tribe_id)
    message = Message.objects.all().filter(pk = message_id)
    user = request.user

    if request.method == 'POST' and user.is_authenticated and tribe.user == user:
        message.delete()
    return redirect(reverse('tribeDetail', kwargs={'tribe_id' : tribe_id}))

def playlistDetail(request, tribe_id, playlist_id):
    tribe = get_object_or_404(Tribe, pk=tribe_id)
    playlist = get_object_or_404(PlayList, pk = playlist_id)

    get_tracks = Track.objects.all().filter(playlist=playlist)
    tracks = []

    for track in get_tracks:
        track_item = {}
        track_item['track'] = track
        track_item['comments'] = track.comment_set.all()
        tracks.append(track_item)

    context = {
        'tribe': tribe,
        'playlist' : playlist,
        'tracks':tracks
    }

    return render(request, 'playlist_detail.html', context)

@login_required
def addTrack(request, tribe_id, playlist_id):
    tribe = get_object_or_404(Tribe, pk=tribe_id)
    playlist = get_object_or_404(PlayList, pk = playlist_id)
    form = TrackForm(request.POST or None)
    user = request.user 

    if form.is_valid():
        track = form.save(commit=False)
        track.user = user
        track.playlist = playlist
        track.save()
        return redirect(reverse('playlistDetail', kwargs={'tribe_id': tribe_id, 'playlist_id':playlist_id}))

    context = {
        'form' : form
    }
    return render(request, 'add_track.html', context)

@login_required
def vote(request, track_id, upvote):
    track = get_object_or_404(Track, pk=track_id)
    vote = Vote.objects.filter(user=request.user, track=track).first()

    if vote:
        if vote.upvote == upvote:
            vote.delete()
            return None
        else:
            vote.upvote = upvote
    else:
        vote = Vote(user=request.user, track=track, upvote=upvote)
    try:
        vote.full_clean()
        vote.save()
    except Exception as e:
        print(e)
        return None
    else:
        return vote

@login_required
def upvote(request, tribe_id, playlist_id, track_id):
    tribe = get_object_or_404(Tribe, pk=tribe_id)
    user = request.user
    if request.method == 'POST' and request.user.is_authenticated or tribe.user == user or user in tribe.member.all():
        vote(request, track_id, True)
    return HttpResponseRedirect(reverse('playlistDetail', args=(tribe_id, playlist_id)))
    
@login_required
def downvote(request, tribe_id, playlist_id, track_id):
    tribe = get_object_or_404(Tribe, pk=tribe_id)
    user = request.user
    if request.method == 'POST' and request.user.is_authenticated or tribe.user == user or user in tribe.member.all():
        vote(request, track_id, False)
    return HttpResponseRedirect(reverse('playlistDetail', args=(tribe_id, playlist_id) ))

@login_required
def comment(request, tribe_id, playlist_id, track_id):
    tribe = get_object_or_404(Tribe, pk = tribe_id)
    playlist = get_object_or_404(PlayList, pk = playlist_id)
    track = get_object_or_404(Track, pk = track_id)
    tracks = Track.objects.all().filter(playlist= playlist)
    comments = Comment.objects.all().filter(track = track)
    user = request.user
    form = CommentForm(request.POST)

    if request.method == 'POST' and user.is_authenticated or tribe.user == user or user in tribe.member.all():
        if form.is_valid():
            comment = form.save(commit=False)
            comment.user = request.user
            comment.track = track
            comment.image = request.user.profile.profile_pic
            comment.save()
            return redirect(reverse('playlistDetail', kwargs={'tribe_id':tribe.id, 'playlist_id': playlist.id}))
    
    context = {
        'tribe': tribe,
        'playlist': playlist,
        'tracks' : tracks,
        'comments': comments,
        'form' : form
    }
    
    return render(request, 'playlist_detail.html', context)

@login_required
def deleteTrack(request, tribe_id, playlist_id, track_id):
    track = get_object_or_404(Track, pk=track_id)
    if request.method == 'POST' and request.user.is_authenticated or request.user_superuser or track.user == request.user:
        track.delete()
    return redirect(reverse('playlistDetail', kwargs={'tribe_id': tribe_id , 'playlist_id':playlist_id}))

@login_required
def deleteComment(request, tribe_id, playlist_id, comment_id):
    comment = get_object_or_404(Comment, pk=comment_id)
    tribe = get_object_or_404(Tribe, pk = tribe_id)
    user = request.user 

    if request.method == 'POST' and user.is_authenticated and user.is_superuser or tribe.user == user:
        comment.delete()
    
    return redirect(reverse('playlistDetail', kwargs={'tribe_id': tribe_id, 'playlist_id':playlist_id}))