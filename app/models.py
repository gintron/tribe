from django.db import models
from django.contrib.auth.models import User
from django.utils import timezone
from django.conf import settings

class TimeStamped(models.Model):
    created_at = models.DateTimeField(editable=False)
    updated_at = models.DateTimeField(editable=False)

    def save(self, *args, **kwargs):
        if not self.created_at:
            self.created_at = timezone.now()

        self.updated_at = timezone.now()
        return super(TimeStamped, self).save(*args, **kwargs)

    class Meta:
        abstract = True

class Profile(models.Model):
    user = models.OneToOneField(User, null=True, blank=True, on_delete=models.CASCADE)
    profile_pic = models.CharField(max_length=512, default='https://9b16f79ca967fd0708d1-2713572fef44aa49ec323e813b06d2d9.ssl.cf2.rackcdn.com/1140x_a10-7_cTC/NS-WKMAG0730-1595944356.jpg')

class Tribe(models.Model):

    MUSIC_GENRE_TYPE = [
        ('pop', 'pop'),
        ('rock', 'rock'),
        ('folk', 'folk'),
        ('techno', 'techno'),
    ]

    name = models.CharField(max_length=80, unique=True)
    user = models.ForeignKey(User, related_name="user+", on_delete=models.CASCADE)
    music_genre = models.CharField(max_length=50, choices= MUSIC_GENRE_TYPE, default="pop")
    tribe_image = models.CharField(max_length=512, default='https://openai.com/content/images/2020/04/2x-no-mark-1.jpg')
    member = models.ManyToManyField(User, related_name="member+")

    def __str__(self):
        return self.name

class PlayList(TimeStamped):
    tribe = models.ForeignKey(Tribe, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE, blank=True, null=True)
    description = models.TextField(max_length=300)
    name = models.CharField(max_length=100)
    
    def __str__(self):
        return self.name

class Message(TimeStamped):
    tribe = models.ForeignKey(Tribe, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    text = models.CharField(max_length=300)
    profile_pic = models.CharField(max_length=512, default ='https://9b16f79ca967fd0708d1-2713572fef44aa49ec323e813b06d2d9.ssl.cf2.rackcdn.com/1140x_a10-7_cTC/NS-WKMAG0730-1595944356.jpg')

    def __str__(self):
        return self.text

class Track(TimeStamped):
    title = models.CharField(max_length=150)
    playlist = models.ForeignKey(PlayList, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    artist = models.CharField(max_length=150)
    youtube_url = models.CharField(max_length=512)
    duration = models.IntegerField(default=0)
    upvotes = models.IntegerField(default=0)
    downvotes = models.IntegerField(default = 0)

    def upvote_score(self):
        upvotes = self.vote_set.filter(upvote=True).count()
        return upvotes

    def downvote_score(self):
        downvotes = self.vote_set.filter(upvote=False).count()
        return downvotes

    def vote_score(self):
        upvotes = self.vote_set.filter(upvote=True).count()
        downvotes = self.vote_set.filter(upvote=False).count()
        return upvotes - downvotes
    
    def vote_by(self, user):
        if user.is_authenticated:
            return Vote.objects.filter(user=user, track=self).first()
        else:
            return None

    def __str__(self):
        return self.title

class Vote(TimeStamped):
    track = models.ForeignKey(Track, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    upvote = models.BooleanField(null = False, default=True)

    class Meta:
        constraints = [
            models.UniqueConstraint(name='user_vote', fields=['track', 'user'])
        ]
    
    def __str__(self):
        return f"{self.user.username} voted on {self.track.title}"
    
    def downvote(self):
        return not self.upvote
        
class Comment(TimeStamped):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    track = models.ForeignKey(Track, on_delete=models.CASCADE)
    text = models.CharField(max_length=500)
    image = models.CharField(max_length=500, default='https://9b16f79ca967fd0708d1-2713572fef44aa49ec323e813b06d2d9.ssl.cf2.rackcdn.com/1140x_a10-7_cTC/NS-WKMAG0730-1595944356.jpg')

    def __str__(self):
        return self.text