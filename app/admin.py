from django.contrib import admin
from .models import Profile, Tribe, PlayList, Message, Track

admin.site.register(Profile)
admin.site.register(Tribe)
admin.site.register(PlayList)
admin.site.register(Message)
admin.site.register(Track)