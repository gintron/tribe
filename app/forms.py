from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django.forms import ModelForm
from .models import Profile, Tribe, PlayList, Message, Track, Comment
class RegisterForm(UserCreationForm):
    email = forms.EmailField()

    class Meta:
        model = User
        fields = ("username", "email", "password1", "password2")

class EditProfileForm(forms.ModelForm):
    class Meta:
        model = User
        fields = (
            'username',
            'email'
        )
class EditProfileImage(forms.ModelForm):
	class Meta:
		model = Profile
		fields = (
			'profile_pic',
		)
class CreateTribeForm(ModelForm):
    class Meta:
        model = Tribe
        fields = ('name', 'music_genre', 'tribe_image')

class PlaylistForm(ModelForm):
    class Meta:
        model = PlayList
        fields = ['name', 'description']

class MessageForm(ModelForm):
    class Meta:
        model = Message
        fields = ['text']

class TrackForm(ModelForm):
    class Meta:
        model = Track
        fields = ['title', 'artist', 'youtube_url', 'duration']

class CommentForm(ModelForm):
    class Meta:
        model = Comment
        fields = ['text']