"""tribes URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from app.views import index, register, loginF, logoutF, profile, createTribe, joinTribe, tribeDetail, createPlaylist, deletePlaylist, editPlaylist, kickMember, leaveTribe, sendMessage, deleteMessage, playlistDetail, addTrack, upvote, downvote, comment,  deleteTrack, deleteComment

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', index, name='index'),
   
    path('createtribe/', createTribe, name='createTribe'),
    path('jointribe/<int:tribe_id>', joinTribe, name="joinTribe"),
    path('detailtribe/<int:tribe_id>', tribeDetail, name="tribeDetail"),
    path('createplaylist/<int:tribe_id>', createPlaylist, name="createPlaylist"),
    path('deleteplaylist/<int:tribe_id>/<int:playlist_id>', deletePlaylist, name="deletePlaylist"),
    path('editplaylist/<int:tribe_id>/<int:playlist_id>', editPlaylist, name="editPlaylist"),
    path('kickmember/<int:tribe_id>/<str:member_name>', kickMember, name="kickMember"),
    path('leavetribe/<int:tribe_id>/<str:userName>', leaveTribe, name="leaveTribe"),
    path('sendmessage/<int:tribe_id>', sendMessage, name="sendMessage"),
    path('deletemessage/<int:tribe_id>/<int:message_id>', deleteMessage, name="deleteMessage"),
    path('playlistdetail/<int:tribe_id>/<int:playlist_id>', playlistDetail, name="playlistDetail"),
    path('addTrack/<int:tribe_id>/<int:playlist_id>', addTrack, name="addTrack"),
    path('upvote/<int:tribe_id>/<int:playlist_id>/<int:track_id>/', upvote, name='upvote'),
    path('downvote/<int:tribe_id>/<int:playlist_id>/<int:track_id>/', downvote, name='downvote'),
    path('comment/<int:tribe_id>/<int:playlist_id>/<int:track_id>', comment, name='comment'),
    path('deletetrack/<int:tribe_id>/<int:playlist_id>/<int:track_id>', deleteTrack, name='deleteTrack'),
    path('deletecomment/<int:tribe_id>/<int:playlist_id>/<int:comment_id>', deleteComment, name='deleteComment'),

    path('register/', register, name='register'),
    path('login/', loginF, name='login'),
    path('logout', logoutF, name='logout'),
    path('profile/', profile, name='profile'),
]
